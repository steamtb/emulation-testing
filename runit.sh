#!/bin/bash

set -e

mkdir -p ~/.steam-img

if [[ -z $STEAM_KVM ]]; then
  echo "The environment variable STEAM_KVM must point to the steamtb KVM source"
  exit 1
fi

if [[ ! -f ~/.steam-img/fedora-27.qcow2 ]]; then
  wget https://mirror.deterlab.net/rvn/img/fedora-27.qcow2 -O ~/.steam-img/fedora-27.qcow2
fi

rm -f ~/.steam-img/disk.qcow2
qemu-img create -f qcow2 \
  -o backing_file=~/.steam-img/fedora-27.qcow2 \
  ~/.steam-img/disk.qcow2

qemu-system-x86_64 \
  -m 1024 \
  -accel kvm \
  -cpu host \
  -drive file=~/.steam-img/disk.qcow2,index=0,media=disk \
  -netdev user,id=mynet0,net=192.168.76.0/24,dhcpstart=192.168.76.9 \
  -device e1000,id=mynet0,netdev=mynet0 \
  -kernel $STEAM_KVM/arch/x86_64/boot/bzImage \
  -nographic \
  -append "earlyprintk=serial console=tty1 console=ttyS0,115200n8 root=/dev/sda1 rw" 

