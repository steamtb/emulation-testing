#!/bin/bash

if [[ $UID != 0 ]]; then
  echo "must be root"
  exit 1
fi

apt update
apt install -y \
  build-essential \
  libelf-dev

cd /tmp/test/kvm
INSTALL_MOD_STRIP=1 make modules_install
make install
depmod

