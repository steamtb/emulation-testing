#!/bin/bash

if [[ $UID != 0 ]]; then
  echo "must be root"
  exit 1
fi

cd /tmp/test/qemu

# for the dependencies
apt install -y \
  qemu \
  qemu-system-x86 \
  libjpeg62-turbo-dev
