# td-testing

This repository contains scripts to test time dilation. Right now testing is supported for nested QEMU/KVM and [MinnowBoards](https://minnowboard.org). The minnowboard is a really nice development platform for KVM because the Atom processor it has supports VMX and when you crash the kernel it only takes a few seconds to reboot.

The included kernel configuration file supports both the nested QEMU/KVM and minnowboard environments and is meant to be used in conjunction with the kernel sources in the [steamtb kvm repository](https://github.com/steamtb/kvm). Right now the Fedora 27 userspace is known to work in both testing environments. Contributions for other distros welcome.

