topo = {
  name: 'dilation-test',
  nodes: [deb('host')]
}

function deb(name) {
  return {
    name: name,
    image: 'debian-buster-td',
    cpu: { cores: 8, passthru: true },
    memory: { capacity: GB(16) },
    mounts: [{ source: env.PWD+'/..', point: '/tmp/test' }]
  }
}
